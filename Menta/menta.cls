\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{menta}[2021]
\LoadClass[11pt, a4paper, oneside]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{titlesec}
\usepackage{graphicx}
\usepackage{float}
\usepackage{fontspec}
\setmainfont{Arial}

\newcommand{\portada}[5]{
    \pagenumbering{roman}
\thispagestyle{empty}
\vspace*{2mm}
\begin{center}
\Large
\includegraphics[width=0.45\textwidth]{1.png}
\\
\vspace*{15mm}
Proyecto de Innovación\\
\vspace*{8mm}
Convocatoria 2020/2021\\
\vspace*{8mm}
#1\\
\vspace*{8mm}
#2\\
\vspace*{8mm}
#3\\
\vspace*{8mm}
#4\\
\vspace*{8mm}
#5
\end{center}

\newpage
\pagenumbering{arabic}
}
\endinput