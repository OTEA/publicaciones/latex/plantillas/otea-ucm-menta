
<div align="center"> 

# OTEA - UCM - Menta

[![Creado para - Overleaf](https://img.shields.io/badge/Only_for-Overleaf-2ea44f?style=for-the-badge&logo=Overleaf&logoColor=white)](https://www.overleaf.com/project) [![XeLaTeX](https://img.shields.io/badge/XeLaTeX-6EBF51?style=for-the-badge)](https://es.overleaf.com/learn/latex/XeLaTeX)
[![TeXLive - 2020](https://img.shields.io/badge/TeXLive-2020-blue?style=for-the-badge)]() 
[<img src="https://img.shields.io/badge/OTEA-UCM-990033.svg?style=for-the-badge&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfmCgcEHy7NK9mqAAAF2klEQVRIx5WVe1BUZRjGn3P27H1hr6ywwMpFcGFF8IoEgYWiZlZ2w4YuljXVyIzhdLPLWI3VNA01jWSadnUsx8wSvOCtC6amAuFILLjLfbksl4W9nD17zh7O6Q/HSUmaev57v3m+33zvO+/3vhJMqdlQSZ5KW2IhA7n8n/jfsgPEvqeb2i60Ht+xzvTylD5iqvO3Rc5eerjnm0tHb3vf13hfhY+9uVVyY6gGB1ns60/ek/xUs3dV4h0nXsy92C/Oesna5Fmwtii560t6D0I33CCvD0pBkwdW3H7Q+KGtjJCoDQqlPPGszuUYmtAut6w1bi3+4XTxCPHkVC94CHvJ/Y8nfsynjQWD8oJOeX7KIsyZUWSe3Wn0EFGpohSpupI7+jc1l+HyPwHzcQwzSw0fqYwjJy5ViKrUAtpnTlHyh16JdQS5iTTutGtLjDVolxeuvLK57RjckwFbUZedvMNk8Xz/27vaJfqC8V/XvPbgrJmFGFq2/eCpsgx5bohp3WmcpsmRZNeeWjxy4MYalOOAwvoCmTp0vnH3qkrbq5FAuE3kOo+NQH//xmSR5xsmuIw3Vrzj3IkmZYZ1Y6X0kRsBS3FrEXk3H2jZtbxcs8jMOnbtOZola/VdYLviqdlzZZ/VdHxh5qMLS9a5PpUx2gfW55Ven8JqvEVt2KyY76mOpuOfGCOkhNucVGKJ5RbELuTRPZxut5Wr85OmBwld6lidXNDODvOrDy8RO64BKlBi022WyN1VSWs8iUZ4SauFjhGmxSxllQypSScMsXNzZgQkenSTJllvtWEZb8qpSR87fi2FJTAXGOOUDu9oyGYBCQOUUuM0VYE+2oI4Qm9QFcTEyKUxIGABM2vA7XdRSQl5hddqkIlUUp0vgjkvl0qjJhABBz8GQIFGN3rBQII+BMCBgwBKSxBCvQLKW+YjEQAFFKAkWpdJYrQxzIfAQokASBBIAYUuEEgChy6woKEBC45gOK4BZeqsdZrcYC8oYBakMQELwwnOvnAao1R74AcJCSYggQIERExAgn6ICCEKYbpvIINUCFSC3cAE94MC4hE0GbTyYLdHyqtpnVqBRDTTPqVAGhEPYBijCAgxbKYyDBWIADkapAwhUqc3mnsAEtBCqYvIGIpe1BPQDpkQByXb4xQgRTIMMGE6pBDEHqc6EgcjogZdYSaXICVyVbTuahHDoIPaK8bo7I1RIt8gAmC9oTopEgHEwwISCaBE5hfWJ0JE5Lxdnl0Rr9K2hWnuKuBrPH763AYdnRqXb+74kWUFDI/zjZLwBMbQjk74IIAKsfUjfhHhkKt6niXJHD1eV762fvdVwD68juHLl9ub9LFFVT/7/oig7wLp4NkeDCMEPzzogRAWmvsbIvCdrzhjKe7UtDgHHBX48Voj1WDT0ODxKFhL04jBg96I+wjfjh4WBAACBFiIXUy7+4iXHzjwmCrjvvmgard4a/7+C4PIgn+04K4Em7n3d5ePOFPVTqeJVLGKGkcABEI0u+VUI98tS+k8lrfc9JjYe/EV19CO6+cBjcrBOw2+QsHef3L/Tk1W4kqhj/SxGQFZAIyf/sJ7Rb3UF7jw7cLMVZuZ6PoPHv5uFJ3XA5x4H06HZsG87JQE8fCA1JQh9wQHNOoMmwFDh0Zq5TziA5dWK+ZWTrcxJ2s33RvaPnkincW2oMthXezLkeUYavQJJWuYz9UdK++3hh3PpXvyXg38Nmdk2S4sbG9teXZx+wMQJwMiqMO2vn1O/a23ZMfmd1czLrdzd9vC7KGWh6ryVIQH4fSqzDlkR8Mza88WYeTm2+Q2iNhf3Nt8Rjzprf9wWyaI8rT1KSA/yWqs6vLVir9f2lcoYh7+RVaI2Grfe9AdYcRz7qbPjqypLWv66kT/sMhw576vtIkwT15hkxFavIcu7YpHNes16emEQyAxk2wU/K1CVfWeFP8G/CelAXgj6efnu083e1tGu3/9qeJNK5B0U+9UyxUKhPGsfp6dEC62bB+XgZvC9xefu4SMIdGOAgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0xMC0wN1QwNDozMTo0NiswMDowMOtRMCgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMTAtMDdUMDQ6MzE6NDYrMDA6MDCaDIiUAAAAV3pUWHRSYXcgcHJvZmlsZSB0eXBlIGlwdGMAAHic4/IMCHFWKCjKT8vMSeVSAAMjCy5jCxMjE0uTFAMTIESANMNkAyOzVCDL2NTIxMzEHMQHy4BIoEouAOoXEXTyQjWVAAAAAElFTkSuQmCC">](<LINK>)

![Menta Título](https://i.ibb.co/ryF9XHY/MENTA.png)

</div>

Plantilla LaTeX para la redacción de memorias en la Universidad Complutense de Madrid, especialmente las de los proyectos  **Innova-Docencia**

# Antecedentes

Esta plantilla es el resultado de una petición particular para la creación de una plantilla para **Overleaf** bajo los proyectos de **Innova-Docencia**.

# Instalación simplificada

Para un uso simplificado puedes pulsar nuestro botón y automáticamente te abrirá
la plantilla en **Overleaf**.

<div align="center"> 

[![Abrir Plantilla - Overleaf](https://img.shields.io/badge/Usar_Plantilla-Menta-2ea44f?style=for-the-badge&logo=Overleaf&logoColor=white)](https://www.overleaf.com/latex/templates/otea-ucm-menta/cfmxhchwgcxy)

</div>

# Instalación y descarga manual

Descargar el archivo .zip **[menta-plantilla.zip](https://gitlab.com/OTEA/publicaciones/latex/plantillas/otea-ucm-menta/-/blob/main/menta-plantilla.zip)** y **no descomprimir**.

El archivo está preparado para **Overleaf** con **XeLaTeX** y **TeXLive 2020**.

Arrastrar el .zip a Overleaf cuando pulsemos la opción "upload a file".

Es importante cambiar las opciones del compilador cuando nos metamos para que funcione.

Puedes encontrar la versión más reciente en [releases](https://gitlab.com/OTEA/publicaciones/latex/plantillas/otea-ucm-menta/-/releases)

# Condiciones

Las plantillas están generadas para su uso en **Overleaf** principalmente para poder compartir los trabajos entre personas de forma sencilla y para poder unificar todas las instalaciones. De esa forma independientemente del sistema operativo y versiones del compilador instalado sabemos que funciona y se puede actualizar.

Respetamos su uso en otras plataformas y de forma local pero **No se garantiza su uso correcto en otra versiones ni en sistemas fuera del cloud.**

# Comandos específicos

Los comandos creados en esta plantilla son los siguientes:

- Comando **portada** con 5 elementos para agregar, este comando tiene que ir en el comienzo del documento, justo debajo del comando **\begin{document}**:
 ```LaTeX
 \portada{Nº del proyecto: \\INTRODUCE TU NUMERO AQUÍ}
 {Título del proyecto: \\INTRODUCE EL TÍTULO DE TU PROYECTO}
 {Nombre del responsable del proyecto: \\INTRODUCE EL NOMBRE}
 {Centro: \\INTRODUCE EL NOMBRE DEL CENTRO}
 {Departamento: \\INTRODUCE EL DEPARTAMENTO} 

 ```

# Licencias

<div align="center">

[![Documentación Licencia: CC BY-SA 4.0](https://img.shields.io/badge/Documentación_Bajo_Licencia-CC_BY--SA_4.0-lightgrey.svg?style=for-the-badge&)](https://creativecommons.org/licenses/by-sa/4.0/)
[![CÓDIGO: MIT](https://img.shields.io/badge/Código_Bajo_Licencia-MIT-yellow.svg?style=for-the-badge&)](https://opensource.org/licenses/MIT)
[![CÓDIGO: MIT](https://img.shields.io/badge/Última_Modificación-PASCAL_2020-green.svg?style=for-the-badge&)](https://opensource.org/licenses/MIT)
</div>
